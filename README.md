files:

README.md (this file)
wwf.py: AI program
words_with_friends_dict.txt: real dictionary
tiny_dict.txt: test dictionary
simple.in: test user input

usage:

$ python -m main.wwf path_to_dictionary_file

# this reads the game input file `command file` and then reads from the connected TTY.
$ python -m main.wwf path_to_dictionary_file -c command_file


TODO:
  * don't return '' on EOFError (main/utility.py:52)
  * support tree add word