import logging
from collections import Counter

from main import utility

class WWFTree:
    def __init__(self, dictionary):
        logging.info('rotating dictionary')
        rotated_dictionary = set([])
        for word in dictionary:
            for pre_post in self._split_word(word):
                rotated_dictionary.add(pre_post)
        logging.info('building tree')
        self._root = WWFNode('', rotated_dictionary, True)
        logging.info('done building tree. added {0} rotations'
                     .format(len(rotated_dictionary)))

    def _split_word(self, word):
        for i in xrange(len(word) + 1):
            yield word[:i], word[i:]

    def get_words(self, include_index=0, fixed_tiles=[None], using_tiles=None):
        if len(fixed_tiles) == 0:
            raise ValueError('Must have at least one fixed tile')
        else:
            if include_index >= len(fixed_tiles):
                raise ValueError('include_index must be inside fixed_tiles list')
        tile_counter = Counter(using_tiles) if using_tiles is not None else None
        fixed_pre = fixed_tiles[:include_index]
        fixed_post = fixed_tiles[include_index+1:]
        including = fixed_tiles[include_index]
        return self._root.get_words(tile_counter, fixed_pre, [including], fixed_post, '')

    def __repr__(self):
        return str(self._root)

    def __str__(self):
        return self.__repr__()            

class WWFNode:
    words_done = 0
    def __init__(self, infix, rotated_dictionary, is_forward):
        self._infix = infix
        self._is_forward = is_forward
        try:
            rotated_dictionary.remove(('', ''))
            if WWFNode.words_done % 10000 == 0:
                logging.info('{0} rotations added'.format(WWFNode.words_done))
            WWFNode.words_done += 1
            self._is_word = True
        except KeyError:
            self._is_word = False

        backward_dicts = {}
        forward_dicts = {}
        if self._is_forward:
            for pre, post in rotated_dictionary:
                if post == '':
                    child_key = pre[-1]
                    if child_key not in backward_dicts:
                        backward_dicts[child_key] = set([])
                    backward_dicts[child_key].add((pre[:-1], ''))
                else:
                    child_key = post[0]
                    if child_key not in forward_dicts:
                        forward_dicts[child_key] = set([])
                    forward_dicts[child_key].add((pre, post[1:]))
        else:
            for pre, post in rotated_dictionary:
                assert post == ''
                child_key = pre[-1]
                if child_key not in backward_dicts:
                    backward_dicts[child_key] = set([])
                backward_dicts[child_key].add((pre[:-1], ''))

        self._forward_children = {
            k: WWFNode(self._infix + k, forward_dicts[k], True)
            for k in forward_dicts 
        }

        self._backward_children = {
            k: WWFNode(k + self._infix, backward_dicts[k], False)
            for k in backward_dicts 
        }

    def get_words(self, using_tiles, fixed_pre, including, fixed_post, infix_spelling):
        """
        :param using_tiles: bag of letters that can be added
        :param fixed_pre: list of letters that must be in the word
                that come before those of the infix (already played).
            They are in order, with correct relative positions, with the end coming
            right before the beginning of the infix.
            None indicates any letter
             may be placed.
        be included, or empty list if it has been included.
        Comes right between fixed_pre and fixed_post
        :param including: letter (or none) to include, or empty list.
            If not empty, then infix_spelling must be empty.
            In this case, goes between fixed_pre and fixed_post.
        :param fixed_post: list of letters that must be in the word
                that come after those of the infix (already played).
            They are in order, with correct relative positions, with the
             beginning coming right after the end of the infix.
            None indicates any letter
             may be placed.
        :param infix_spelling: the tiles that spell the infix around which to continue

        :return: set of tuples (word, spelling used, start_index)
            (may be multiple ways to spell a word using blanks, even using the same set of letters.
            Also a word may be placed in two places, e.g.
                fixed = [None, None, s, None] -> [None, a, s, s] or
                [a, s, s, None])
        """
        words = set([])
        if self._is_word and len(including) == 0:
            # already included the letter
            clear_ends = (len(fixed_pre) == 0 or fixed_pre[-1] is None) and \
                         (len(fixed_post) == 0 or fixed_post[0] is None)
            if clear_ends:
                assert utility.spells_word(self._infix, infix_spelling)
                words.add((self._infix, infix_spelling, len(fixed_pre)))

        if len(including) > 0:
            assert infix_spelling == '' # the "including" list is not defined relative to infix -
                # only to pre and post. Therefore output is undefined when including is not empty
                # and infix is also not empty; but when infix is empty it is well-defined.
            words.update(self._get_words_from_children(
                using_tiles, including[0], True, fixed_pre, fixed_post, infix_spelling
            ))
        else:
            # including is empty, which means we can go forward or back
            if len(fixed_post) > 0:
                words.update(self._get_words_from_children(
                    using_tiles, fixed_post[0], True, fixed_pre, fixed_post[1:], infix_spelling
                ))
            if len(fixed_pre) > 0:
                words.update(self._get_words_from_children(
                    using_tiles, fixed_pre[-1], False, fixed_pre[:-1], fixed_post, infix_spelling
                ))
        return words

    def _get_words_from_children(self, using_tiles, letter, go_forward, fixed_pre, fixed_post, infix_spelling):
        children = self._forward_children if go_forward else self._backward_children
        if letter is not None:
            children_to_search = {(letter, '', children[letter])} if letter in children else set([])
        else:
            if using_tiles is None:
                children_to_search = {(l, '', children[l]) for l in children}
            else:
                children_to_search = set([])
                if '*' in using_tiles:
                    children_to_search.update({('*', '*', children[l]) for l in children})
                children_to_search.update(
                    {(l, l, children[l]) for l in children if l in using_tiles}
                )
        words = set([])
        for letter_spelling, using_tile, child in children_to_search:
            if go_forward:
                new_infix_spelling = infix_spelling + letter_spelling
            else:
                new_infix_spelling = letter_spelling + infix_spelling
            using_rest_of_tiles = using_tiles - Counter(using_tile) if using_tiles is not None else None
            words.update(child.get_words(using_rest_of_tiles,
                                         fixed_pre, [], fixed_post, new_infix_spelling))
        return words

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return self.__repr__rec(0)

    def __repr__rec(self, level):
        f = 'f' if self._is_forward else 'b'
        s = '\n' + level * '  ' + '{0}|{1}|{2}|'.format(f, self._infix, self._is_word)
        s += '\n'.join([child.__repr__rec(level + 1) for child in self._forward_children.values()]) \
           + '\n'.join([child.__repr__rec(level + 1) for child in self._backward_children.values()])
        return s

if __name__ == '__main__':
    dic = set([''])
    t = WWFTree(dic)
    print str(t)
