import random
import unittest

import itertools

from main import utility
from wwf_tree.wwf_tree import WWFTree

class WWFTreeTests(unittest.TestCase):
    def setUp(self):
        pass

    def test_empty_dict(self):
        dic = set([])
        t = WWFTree(dic)
        self.assertEqual(t.get_words(0, [None], []), set())
        self.assertEqual(t.get_words(0, ['c', None, 't'], 'a'), set())
        self.assertEqual(t.get_words(0, [None], 'aab*'), set())

    def test_simple(self):
        dic = set(['dick'])
        t = WWFTree(dic)
        full_ans = {('dick', 'dick', 0)}
        self.assertEqual(t.get_words(0, [None]*4, None), full_ans)
        self.assertEqual(t.get_words(0, [None]*4, 'ickd'), full_ans)
        self.assertEqual(t.get_words(0, ['d']+[None]*3, 'kci'), full_ans)
        self.assertEqual(t.get_words(0, [None, 'i', None, 'k'], 'dc'), full_ans)
        self.assertEqual(t.get_words(0, ['d', 'i', 'c', 'k'], ''), full_ans)

    def test_branching(self):
        dic = set(['b', 'ab', 'bc', 'abc'])
        t = WWFTree(dic)
        self.assertEqual(t.get_words(1, [None, 'b', None], None),
                         {
                             ('b', 'b', 1), ('ab', 'ab', 0),
                             ('bc', 'bc', 1), ('abc', 'abc', 0)
                         })
        self.assertEqual(t.get_words(1, [None, 'b', None], 'ac'),
                         {
                             ('b', 'b', 1), ('ab', 'ab', 0),
                             ('bc', 'bc', 1), ('abc', 'abc', 0)
                         })
        self.assertEqual(t.get_words(0, ['a', None, 'c'], 'abc'),
                         {
                             ('abc', 'abc', 0)
                         })
        self.assertEqual(t.get_words(2, [None, 'b', 'c'], 'a'),
                         {
                             ('bc', 'bc', 1), ('abc', 'abc', 0)
                         })
        self.assertEqual(t.get_words(0, ['a', 'b', None], 'c'),
                         {
                             ('ab', 'ab', 0), ('abc', 'abc', 0)
                         })

    def test_wildcards_simple(self):
        dic = set(['at', 'cat'])
        t = WWFTree(dic)
        self.assertEqual(t.get_words(1, ['c', None, 't'], 'a*'),
                         {
                             ('cat', 'cat', 0),
                             ('cat', 'c*t', 0)
                         })
        self.assertEqual(t.get_words(1, [None, 'a', None], 'at*'),
                         {
                             ('at', 'at', 1),
                             ('cat', '*at', 0),
                             ('at', 'a*', 1)
                         })

    def test_use_only_some_fixed(self):
        dic = set(['able', 'rub', 'egg'])
        t = WWFTree(dic)
        self.assertEqual(t.get_words(1, [None, None, 'b', None, 'e', None, None], 'alrugg'),
                         {
                             ('able', 'able', 1),
                             ('rub', 'rub', 0)
                         })
        self.assertEqual(t.get_words(4, [None, None, 'b', None, 'e', None, None], 'alrugg'),
                         {
                             ('able', 'able', 1),
                             ('egg', 'egg', 4)
                         })
        self.assertEqual(t.get_words(0, [None, None, 'b', None, 'e', None, None], 'alrug'),
                         {
                             ('rub', 'rub', 0)
                         })
        self.assertEqual(t.get_words(3, [None, 'b', None, 'e', None, None], 'lrugg'),
                         {
                             ('egg', 'egg', 3)
                         })

    def test_word_ends_before_include(self):
        dic = set(['bhai'])
        t = WWFTree(dic)
        self.assertEqual(t.get_words(4, [None, None, None, 'i', None, None], 'bcafhed'),
                         set([]))

    def test_two_start_indices(self):
        dic = set(['ass'])
        t = WWFTree(dic)
        self.assertEqual(t.get_words(1, [None, None, 's', None], 'as'),
                         {
                             ('ass', 'ass', 0), ('ass', 'ass', 1)
                         })

    def test_play_no_tiles(self):
        dic = set(['abc'])
        t = WWFTree(dic)
        self.assertEqual(t.get_words(0, ['a', 'b', 'c'], ''),
                         {
                             ('abc', 'abc', 0)
                         })

    def _get_words_reference(self, dic, including, fixed_tiles, using_tiles):
        res = set()
        for word in dic:
            for e1 in xrange(0, including + 1):
                e2 = e1 + len(word) - 1
                if e2 > len(fixed_tiles) - 1:
                    continue
                if not (e1 <= including <= e2):
                    continue
                if not self._clear_ends(fixed_tiles, e1, e2):
                    continue
                sub_fixed = fixed_tiles[e1:e2+1]
                num_to_use = sub_fixed.count(None)
                if num_to_use == 0:
                    continue
                for perm in itertools.permutations(using_tiles, num_to_use):
                    # copy fixed_tiles and then fill in gaps
                    fixed_filled = [el for el in fixed_tiles]
                    perm_index = 0
                    for i in xrange(e1, e2 + 1):
                        if fixed_tiles[i] is not None:
                            fixed_filled[i] = fixed_tiles[i]
                        else:
                            fixed_filled[i] = perm[perm_index]
                            perm_index += 1
                    assert perm_index == num_to_use
                    assert len(fixed_filled) == len(fixed_tiles)
                    assert fixed_filled != fixed_tiles
                    sub_fixed_filled = fixed_filled[e1:e2+1]
                    spelling = ''.join(sub_fixed_filled)
                    if utility.spells_word(word, spelling):
                        res.add((word, spelling, e1))
        return res

    def test_oracle(self):
        random.seed(1111)
        alphabet = 'abcdefghij'
        none_ratio = 3 # 3 times as many Nones as fixed letters
        for i in xrange(100):
            dic = {
                ''.join(random.choice(alphabet)
                    for _ in range(random.randint(2, 8))) # min word length = 2
                        for _ in range(200)
            }
            t = WWFTree(dic)
            for j in xrange(1):
                fixed = [random.choice(list(alphabet) + [None] * len(alphabet) * none_ratio) \
                         for _ in range(random.randint(4,9))]
                using = [random.choice(list(alphabet) + ['*']) for _ in range(7)]
                including = 0 if len(fixed) == 0 else random.randint(0, len(fixed) - 1)

                test_res = t.get_words(including, fixed, using)
                ref_res = self._get_words_reference(dic, including, fixed, using)
                self.assertEqual(test_res, ref_res)

    def _clear_ends(self, filled_letters, e1, e2):
        if e1 > 0 and filled_letters[e1 - 1] is not None:
            return False
        if e2 < len(filled_letters) - 1 and filled_letters[e2 + 1] is not None:
            return False
        return True


if  __name__ == '__main__':
    unittest.main()