import heapq
import logging
from argparse import ArgumentParser

import re
from colorama import Fore, Back

from utility import *
from wwf_tree.wwf_tree import WWFTree


class Move(SmartHashable):
    def __init__(self, word, spelling, orientation, start_xy):
        super(Move, self).__init__({'_trace'})
        if '*' in word:
            raise ValueError('wildcards not allowed in moves')
        if orientation not in ['h', 'v']:
            raise ValueError('orientation must be h or v')
        try:
            x, y = start_xy
            i_x, i_y = int(x), int(y)
        except ValueError:
            raise ValueError('coordinates must be integers')
        if i_x < 0 or i_y < 0:
            raise ValueError('coordinates must be non-negative')
        if not spells_word(word, spelling):
            raise ValueError('invalid tiles for move')
        self.word = word
        self.orientation = orientation
        self.start_xy = start_xy
        self._trace = self._create_trace()
        self.spelling = spelling

    def get_trace(self):
        return self._trace

    def _create_trace(self):
        x, y = self.start_xy
        if self.orientation == 'v':
            return [(x + i, y) for i in range(len(self.word))]
        else:
            return [(x, y + i) for i in range(len(self.word))]

    def iter_word_trace(self):
        return zip(self.word, self.get_trace())

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        x, y = self.start_xy
        if self.orientation == 'v':
            return '\n'.join(['({0}, {1}) {2}'
                             .format(x + i, y, w)
                              for i, w in enumerate(self.word)
                              ])
        else:
            return ' '.join(['({0}, {1}) {2}'
                            .format(x, y + i, w)
                             for i, w in enumerate(self.word)
                             ])


class Board(SmartHashable):
    def __init__(self, size, double_letters, triple_letters, double_words, triple_words, grid=None):
        self._double_letters = double_letters
        self._triple_letters = triple_letters
        self._double_words = double_words
        self._triple_words = triple_words
        self.size = size
        self._last_move = None
        self._highlight_last_move = False
        if grid is None:
            self._grid = [[None for _ in xrange(self.size)] for _ in xrange(self.size)]
        else:
            grid_copy = [[grid[i][j] for j in range(self.size)] for i in range(self.size)]
            self._grid = grid_copy

    def copy(self):
        return Board(self.size, self._double_letters, self._triple_letters,
                     self._double_words, self._triple_words, grid=self._grid)

    def clear(self):
        self._grid = [[None for _ in xrange(self.size)] for _ in xrange(self.size)]

    def is_center(self, i, j):
        return i == j == (self.size - 1) / 2

    def enumerate_possible_included_squares(self):
        if self._is_empty():
            c_x, c_y = (self.size - 1) / 2, (self.size - 1) / 2
            yield (c_x, c_y)
        else:
            for i in xrange(self.size):
                for j in xrange(self.size):
                    if self.get_square_letter((i, j)) is not None:
                        yield (i, j)
                        for i2, j2 in self._get_orth_neighbors(i, j):
                            yield (i2, j2)

    def get_needed_tile_indices(self, move):
        if not self._is_on_board(move):
            raise IllegalMoveException('move is not on board')
        if self._conflicts(move):
            raise IllegalMoveException('move conflicts with board')
        if not self._has_clear_ends(move):
            raise IllegalMoveException('ends are not clear')
        needed_indices = set([])
        for i, sq in enumerate(move.get_trace()):
            if self.get_square_letter(sq) is None:
                needed_indices.add(i)

        return needed_indices

    def _get_orth_neighbors(self, i, j):
        ns = set([])
        if self._sq_is_on_board((i + 1, j)):
            ns.add((i + 1, j))
        if self._sq_is_on_board((i - 1, j)):
            ns.add((i - 1, j))
        if self._sq_is_on_board((i, j + 1)):
            ns.add((i, j + 1))
        if self._sq_is_on_board((i, j - 1)):
            ns.add((i, j - 1))
        return ns

    def do_move(self, move, check_for_disconnect):
        if self.is_legal_move(move, check_for_disconnect):
            needed = [move.word[i] for i in
                      self.get_needed_tile_indices(move)]
            for i in xrange(len(move.word)):
                x, y = move.get_trace()[i]
                l = move.word[i]
                t = move.spelling[i]
                self._grid[x][y] = (l, t)
            self._last_move = move
            return needed
        else:
            raise IllegalMoveException('Board: cannot do illegal move')

    def requires_new_tiles(self, move):
        return len(self.get_needed_tile_indices(move)) > 0

    def is_legal_move(self, move, check_for_disconnect):
        # get_needed_tile_indices covers most of the conditions
        # also check that at least one tile is played and that the move connects to the rest of the board
        return self.requires_new_tiles(move) and \
               (not check_for_disconnect or self.connects(move))

    def read_letters_from_trace(self, trace):
        return [self.get_square_letter(sq) for sq in trace]

    def connects(self, move):
        """
        :param move: move must be on board, not conflict, and have clear ends.
        :return:
        """
        if self._is_empty():
            return self._contains_center(move)
        else:
            if len(self.get_indexed_orth_traces(move)) > 0:
                return True
        return any([l is not None for l in self.read_letters_from_trace(move.get_trace())])

    def get_square_letter(self, coords):
        """

        :rtype: string
        """
        x, y = coords
        if self._grid[x][y] is None:
            return None
        return self._grid[x][y][0]

    def get_square_tile(self, coords):
        """

        :rtype: string
        """
        x, y = coords
        if self._grid[x][y] is None:
            return None
        return self._grid[x][y][1]

    def get_indexed_orth_traces(self, move):
        """
        :param move: move must be on board, not conflict, and have clear ends.
        :return: list of tuples (trace, index_in_main, index_in_orth)
        index_in_main is the index of the shared tile in the main word
        index_in_orth is the index of the shared tile in the orth word

        NOTE: includes all the orthogonal words, excluding 1-letter but
        including ones not played this turn.
        """
        with_move = self.try_move(move, False)  # do not need to check that it is connected
        # because orthogonal word traces is defined either way
        orth_dir = 'h' if move.orientation == 'v' else 'v'

        # orthogonal words of lenght 1 are just the square itself and don't count.
        indexed_traces = []
        for i, sq in enumerate(move.get_trace()):
            orth_trace, index_in_orth = with_move._get_trace_from_inner_sq(sq, orth_dir)
            if len(orth_trace) > 1:
                indexed_traces.append((orth_trace, i, index_in_orth))

        return indexed_traces

    def _get_trace_from_inner_sq(self, sq, o):
        """
        :param sq: a square within a word that is being tried.
         is on the grid and contains a letter.
        :param o: orientation to follow
        :return:
        """
        f_sq = l_sq = end_1 = end_2 = sq
        index_of_start_sq = -1  # needs to start at -1, not 0
        # find the ends (and their respective first and last squares, just inside
        # first end
        while self._sq_is_on_board(end_1) and self.get_square_letter(end_1) is not None:
            f_sq = end_1
            index_of_start_sq += 1
            if o == 'v':
                end_1 = (end_1[0] - 1, end_1[1])
            else:
                end_1 = (end_1[0], end_1[1] - 1)
        # last end
        while self._sq_is_on_board(end_2) and self.get_square_letter(end_2) is not None:
            l_sq = end_2
            if o == 'v':
                end_2 = (end_2[0] + 1, end_2[1])
            else:
                end_2 = (end_2[0], end_2[1] + 1)
        return self.get_trace_between(f_sq, l_sq), index_of_start_sq

    def get_trace_between(self, sq_1, sq_2):
        """
        :param sq_1: the left or top square
        :param sq_2: the right or bottom square
        :return: the trace between them (including both)
        """
        orient = self._get_trace_orientation(sq_1, sq_2)
        if sq_1[1] == sq_2[1]:
            # v
            assert orient == 'v'
            return [(i, sq_1[1]) for i in range(sq_1[0], sq_2[0] + 1)]
        elif sq_1[0] == sq_2[0]:
            assert orient == 'h'
            # h
            return [(sq_1[0], j) for j in range(sq_1[1], sq_2[1] + 1)]
        else:
            raise ValueError('invalid end squares: would form diagonal trace')

    @staticmethod
    def _get_trace_orientation(trace_end_1, trace_end_2):
        sq_1 = trace_end_1
        sq_2 = trace_end_2
        if sq_1[1] == sq_2[1]:
            return 'v'
        elif sq_1[0] == sq_2[0]:
            return 'h'
        else:
            raise ValueError('invalid trace: diagonal')

    def try_move(self, move, check_for_disconnect=True):
        """
        :param move: move must be on board, not conflict, and have clear ends.
        :param check_for_disconnect whether to check if the move being tried connect to
            tiles on the board
        :return: A new copy of this board with the move done
        """
        b = self.copy()
        b._highlight_last_move = True
        b.do_move(move, check_for_disconnect)
        return b

    def _is_empty(self):
        for i in xrange(self.size):
            for j in xrange(self.size):
                if self._grid[i][j] is not None:
                    return False
        return True

    def _conflicts(self, move):
        """
        :param move: must be on the board
        :return:
        """
        for w, sq in move.iter_word_trace():
            existing_letter = self.get_square_letter(sq)
            if existing_letter is not None and existing_letter != w:
                return True
        return False

    def _is_on_board(self, move):
        return self._sq_is_on_board(move.get_trace()[0]) and \
               self._sq_is_on_board(move.get_trace()[-1])

    def _has_clear_ends(self, move):
        """
        :param move: must be on the board and not conflicting
        :return:
        """

        last_xy = move.get_trace()[-1]
        if move.orientation == 'v':
            end_1 = (move.start_xy[0] - 1, move.start_xy[1])
            end_2 = (last_xy[0] + 1, last_xy[1])
        else:
            end_1 = (move.start_xy[0], move.start_xy[1] - 1)
            end_2 = (last_xy[0], last_xy[1] + 1)

        # both ends must be clear, i.e. off the board or None
        return (not self._sq_is_on_board(end_1) or self.get_square_letter(end_1) is None) and \
               (not self._sq_is_on_board(end_2) or self.get_square_letter(end_2) is None)

    def _sq_is_on_board(self, coords):
        x, y = coords
        return 0 <= x < self.size and 0 <= y < self.size

    def _contains_center(self, move):
        trace = move.get_trace()

        center_coor = (self.size - 1) / 2
        f_sq = trace[0]
        l_sq = trace[-1]
        trace_orientation = self._get_trace_orientation(f_sq, l_sq)
        assert trace_orientation == move.orientation
        if move.orientation == 'v':
            return (f_sq[0] <= center_coor <= l_sq[0]) and (f_sq[1] == center_coor)
        else:
            return (f_sq[1] <= center_coor <= l_sq[1]) and (f_sq[0] == center_coor)

    def score_legal_done(self, trace, new_indices, tile_vals):
        """
        :param trace: trace representing a legal move
        :param new_indices: indices within the move being played this turn
        :param tile_vals dict from tile -> number value
        :return: the score of this word
        """
        # try_move redundantly checks legality
        base_score = 0
        word_mult = 1
        for i, sq in enumerate(trace):
            letter_mult = 1
            if i in new_indices:
                # apply special square if applicable
                if sq in self._double_words:
                    word_mult *= 2
                if sq in self._triple_words:
                    word_mult *= 3
                if sq in self._double_letters:
                    letter_mult = 2
                if sq in self._triple_letters:
                    letter_mult = 3
            tile = self.get_square_tile(sq)
            base_score += letter_mult * tile_vals[tile]
        return base_score * word_mult

    def get_orth_words(self, move):
        """
        
        :param move: legal except possibly for orth non-words 
        :return: the orth words (and/or non-words)
        """
        indexed_traces = self.get_indexed_orth_traces(move)
        with_move = self.try_move(move, False)  # orthogonal words is defined if move does not connect
        return {''.join(with_move.read_letters_from_trace(tup[0])) for tup in indexed_traces}

    def _cell_str(self, i, j):
        if self._highlight_last_move and (i, j) in self._last_move.get_trace():
            t_color = Back.LIGHTYELLOW_EX
        else:
            t_color = Back.YELLOW
        l = self.get_square_letter((i, j))
        t = self.get_square_tile((i, j))
        l_color = Fore.LIGHTBLACK_EX if t == '*' else Fore.BLACK

        if l is None:
            if (i, j) in self._double_letters:
                v = 'DL'
                l_color = Fore.WHITE
                sq_color = Back.BLUE
            elif (i, j) in self._triple_letters:
                v = 'TL'
                l_color = Fore.WHITE
                sq_color = Back.GREEN
            elif (i, j) in self._double_words:
                v = 'DW'
                l_color = Fore.WHITE
                sq_color = Back.RED
            elif (i, j) in self._triple_words:
                v = 'TW'
                l_color = Fore.WHITE
                sq_color = Back.YELLOW
            else:
                v = '  '
                l_color = Fore.WHITE
                sq_color = Back.WHITE
            if self.is_center(i, j):
                sq_color = Back.CYAN
                v = '  '
                l_color = Fore.BLACK
        else:
            v = l + ' '
            sq_color = t_color

        return colored(v, l_color, sq_color)

    @staticmethod
    def get_start_sq(inner_sq, offset, o):
        x, y = inner_sq
        if o == 'h':
            y -= offset
        else:
            x -= offset
        return x, y

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        def adjusted_len(el):
            if isinstance(el, str):
                contains_light_control_seq = False
                for cs in {Back.LIGHTYELLOW_EX}:
                    if cs in el:
                        contains_light_control_seq = True
                if contains_light_control_seq:
                    # element is a string containing a LIGHT sequence code
                    return len(el) - 1
            return len(str(el))

        max_num_len = max([adjusted_len(i) for i in range(self.size)])
        s = ' ' * max_num_len + ''.join([str(i) + '_' * (max_num_len - len(str(i))) for i in range(self.size)]) + '\n'
        m = [[i] + [self._cell_str(i, j) for j in xrange(self.size)] + ['|']
             for i in xrange(self.size)]
        lens = [max(map(adjusted_len, col)) for col in zip(*m)]
        fmt = ''.join('{{:{}}}'.format(x) for x in lens)
        table = [fmt.format(*row) for row in m]
        s += '\n'.join(table)
        s += '\n' + '_' * (1 + self.size) * 2
        return s


class Player():
    def __init__(self, name, io=None, wants_recommendations=True, infinite_bag=False):
        self._name = name
        self.set_io(io)
        self.wants_recommendations = wants_recommendations
        self._infinite_bag = infinite_bag
        if not self._infinite_bag:
            self._tiles = Bag()

    def set_io(self, io):
        self._io = io if io is not None else RawInputCommentPrint()

    def reset(self):
        if not self._infinite_bag:
            self._tiles = Bag()

    def report_move(self, game, ignore_dict=False):
        while True:
            try:
                prompt = '{0} played? [word],[tiles],[h|v],[start_x],[start_y]\n'.format(str(self)) \
                         + '(\s for swap)\n'
                inp = self._io.raw_input(prompt)
                ans = [p.strip() for p in inp.split(',')]
                if not ans[0].startswith('\\s'):
                    # not a swap
                    move = Move(ans[0], ans[1], ans[2], (int(ans[3]), int(ans[4])))
                    if game.is_legal_move(move, self, ignore_dict=ignore_dict):
                        self._replace_tiles(game.do_move(move, True))
                    else:
                        logging.debug("illegal move:" + str(move))
                        raise IllegalMoveException()
                else:
                    self._replace_tiles(ans[1])
                return True
            except IllegalMoveException:
                print 'illegal move.'
            except (ValueError, IndexError):
                print 'bad tiles or formatting: '

    def can_play(self, move, game):
        needed_indices = game._board.get_needed_tile_indices(move)
        needed_letters = ''
        for i, l in enumerate(move.spelling):
            if i in needed_indices:
                needed_letters += l
        return self._infinite_bag or self._tiles.contains(Bag(needed_letters))

    def get_tiles(self):
        return self._tiles.copy() if not self._infinite_bag else None

    def tiles_str(self):
        if self._infinite_bag:
            return '[unlimited tiles]'
        else:
            return str(self._tiles)

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return '\ncurrent board:\n' + str(self._board)

    def _verify_hand(self, num_to_hold):
        if not self._infinite_bag:
            num_held = sum(self._tiles.values())
            if num_held != num_to_hold:
                raise ValueError('wrong number of letters in hand: {0}'.format(num_held))

    def _take_tiles(self, word, num_to_hold):
        if not self._infinite_bag:
            self._tiles += Bag(word)

    def _set_tiles(self, word, num_to_hold):
        if not self._infinite_bag:
            self._replace_tiles(self._tiles)
            self._take_tiles(word, num_to_hold)

    def _replace_tiles(self, word):
        if not self._infinite_bag:
            to_replace = Bag(word)
            if self._tiles.contains(to_replace):
                self._tiles -= to_replace
            else:
                raise ValueError('not holding these tiles')

    def report_new_tiles(self, num_to_hold):
        while True:
            try:
                new_tile_input = self._io.raw_input('tiles held now (* for blank): ')
                if not all([t.isalpha() or t == '*' for t in new_tile_input]):
                    raise ValueError('letters or * only')
                tiles = ''.join(re.findall('[\*a-zA-Z]+', new_tile_input)).lower()
                self._set_tiles(tiles, num_to_hold)
                return True
            except ValueError as e:
                print e.message + '. Try again.'

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return 'player \"{0}\"'.format(self._name)

class Game():
    DEFAULT_TILE_COUNTS = Bag(
        'a' * 9 + 'b' * 2 + 'c' * 2 + 'd' * 5 + 'e' * 13 + 'f' * 2 + 'g' * 3 + 'h' * 4 + 'i' * 8 + 'j' * 1 +
        'k' * 1 + 'l' * 4 + 'm' * 2 + 'n' * 5 + 'o' * 8 + 'p' * 2 + 'q' * 1 + 'r' * 6 + 's' * 5 + 't' * 7 +
        'u' * 4 + 'v' * 2 + 'w' * 2 + 'x' * 1 + 'y' * 2 + 'z' * 1 + '*' * 2)
    DEFAULT_TILE_VALS = {
        'a': 1, 'b': 4, 'c': 4, 'd': 2, 'e': 1, 'f': 4, 'g': 3,
        'h': 3, 'i': 1, 'j': 10, 'k': 5, 'l': 2, 'm': 4, 'n': 2,
        'o': 1, 'p': 4, 'q': 10, 'r': 1, 's': 1, 't': 1, 'u': 2,
        'v': 5, 'w': 4, 'x': 8, 'y': 3, 'z': 10, '*': 0
    }
    DEFAULT_DOUBLE_LETTERS = {
        (1, 2), (2, 1), (2, 4), (4, 2), (6, 4), (4, 6),
        (1, 12), (2, 13), (2, 10), (4, 12), (4, 8), (6, 10),
        (12, 1), (13, 2), (10, 2), (12, 4), (8, 4), (10, 6),
        (13, 12), (12, 13), (12, 10), (10, 12), (10, 8), (8, 10)
    }
    DEFAULT_TRIPLE_LETTERS = {
        (0, 6), (6, 0), (3, 3), (5, 5),
        (0, 8), (6, 14), (3, 11), (5, 9),
        (8, 0), (14, 6), (11, 3), (9, 5),
        (14, 8), (8, 14), (11, 11), (9, 9)
    }
    DEFAULT_DOUBLE_WORDS = {
        (1, 5), (5, 1),
        (1, 9), (5, 13),
        (9, 1), (13, 5),
        (13, 9), (9, 13),
        (3, 7), (11, 7), (7, 3), (7, 11)
    }
    DEFAULT_TRIPLE_WORDS = {
        (0, 3), (3, 0),
        (0, 11), (3, 14),
        (11, 0), (14, 3),
        (14, 11), (11, 14)
    }

    def __init__(self, dic, players, io, board_size=15, tile_counts=DEFAULT_TILE_COUNTS,
                 tile_vals=DEFAULT_TILE_VALS, double_letters=DEFAULT_DOUBLE_LETTERS,
                 triple_letters=DEFAULT_TRIPLE_LETTERS, double_words=DEFAULT_DOUBLE_WORDS,
                 triple_words=DEFAULT_TRIPLE_WORDS, num_letters_to_hold=7,
                 num_recommendations=1):
        self._dic = dic
        if len(players) != 2:
            raise ValueError('only 2 player games supported')
        self._players = players
        if board_size % 2 == 0:
            raise ValueError('board has no center square')
        self._board = Board(board_size, double_letters, triple_letters, double_words, triple_words)
        self._tile_counts = tile_counts
        self._tile_vals = tile_vals
        self._tree = WWFTree(self._dic)
        self._num_tiles_to_hold = num_letters_to_hold
        self._num_moves_to_recommend = num_recommendations
        self.set_io(io)

    def set_io(self, io):
        self._io = io
        for p in self._players:
            p.set_io(io)

    def play(self, display=True, ignore_dict=False):
        while True:
            try:
                prompt = 'Who goes first?' + ''.join(['\n{0}: {1}'.format(i, p)
                                              for i, p in enumerate(self._players)]) \
                    + '\nEnter number. '
                inp = self._io.raw_input(prompt)
                player_num = int(inp)
                if player_num < 0 or player_num >= len(self._players):
                    raise ValueError('player number not in range')
                self._game_loop(player_num, display, ignore_dict=ignore_dict)
            except ValueError:
                print 'try again'
            except QuitException:
                raise GameOverException('user ended game.')

    def _game_loop(self, go_first_num, display, ignore_dict=False):
        current_player_num = go_first_num
        assert go_first_num in [0, 1]
        while True:
            current_player = self._players[current_player_num]
            print '{0} goes now.'.format(str(current_player))
            try:
                current_player.report_new_tiles(self._num_tiles_to_hold)
                if display:
                    print str(self)
                if current_player.wants_recommendations and not self._io.has_file_input():
                    # if io can't read more from file, then next input will be interactive
                    self._recommend_move(current_player)
                print 'Tiles held:', current_player.tiles_str()
                current_player.report_move(self, ignore_dict=ignore_dict)
            except NoLegalMovesException:
                raise GameOverException('no legal moves')
            current_player_num = (current_player_num + 1) % len(self._players)

    def do_move(self, move, check_for_disconnect=False):
        self._board.do_move(move, check_for_disconnect)

    def reset(self):
        self._board.clear()
        for p in self._players:
            p.reset()

    def _recommend_move(self, player):
        print 'move suggestions'
        for move, score in self._best_moves(player):
            print 'recommended move ({0} points):'.format(score)
            print self._board.try_move(move)  # try_move redundantly checks legality

    def _best_moves(self, player):
        moves = set([])
        for move in self._enumerate_possible_moves(player.get_tiles()):
            try:
                move_score = self._score(move, player)
            except IllegalMoveException:
                continue
            moves.add((move, move_score))
        if len(moves) == 0:
            raise NoLegalMovesException()
        return heapq.nlargest(self._num_moves_to_recommend, moves, key=lambda x: x[1])

    def _enumerate_possible_moves(self, player_tiles):
        """

        :return: a superset of the legal moves, for which the player has the proper tiles (including wildcards)
        """
        # horizontal, then vertical

        # set of tuples (orientation, x, y)
        # look for all legal moves within an entire row/column
        # where the move must include (x, y)
        positions_tried = set([])
        include_squares = self._board.enumerate_possible_included_squares()
        for x, y in include_squares:
            if ('h', x, y) not in positions_tried:
                row_trace = self._board.get_trace_between((x, 0), (x, self._board.size - 1))
                row_letters = self._board.read_letters_from_trace(row_trace)
                for word, spelling, start_index_y in self._tree.get_words(
                        include_index=y, fixed_tiles=row_letters, using_tiles=player_tiles
                ):
                    yield Move(word, spelling, 'h', (x, start_index_y))
                positions_tried.add(('h', x, y))
            if ('v', x, y) not in positions_tried:
                col_trace = self._board.get_trace_between((0, y), (self._board.size - 1, y))
                col_letters = self._board.read_letters_from_trace(col_trace)
                for word, spelling, start_index_x in self._tree.get_words(
                        include_index=x, fixed_tiles=col_letters, using_tiles=player_tiles
                ):
                    yield Move(word, spelling, 'v', (start_index_x, y))
                positions_tried.add(('v', x, y))

    def _score(self, move, player):
        if self.is_legal_move(move, player):
            score = self._score_legal(move)
            logging.debug('move=\n{0}, score={1}'.format(move, score))
            return score
        raise IllegalMoveException('cannot score illegal move')

    def is_legal_move(self, move, player, ignore_dict=False):
        """

        :param move: A potential move generated by WWFTree. In particular,
            # move has a real word, is on the board, does not conflict, has clear ends,
            and is composed only of tiles the player has.
            # may have orthogonal non-words, not connect to board, or
            # there may be no new tiles needed.
        :return: bool
        """
        # redundant checks
        if not player.can_play(move, self):
            return False
        if move.word not in self._dic and not ignore_dict:
            logging.debug(str(move.word) + " not in dic and ignore_dict==False")
            return False
        if not self._board.requires_new_tiles(move):
            return False
        if not self._board.connects(move):
            return False
        if not self._board.get_orth_words(move) <= self._dic:
            return False
        return True

    def _score_legal(self, move):
        """
        :param move: a legal move for the current board
        :return: the score (accounting for special squares, etc.)
        """
        with_move = self._board.try_move(move)
        new_indices = self._board.get_needed_tile_indices(move)
        main_word_score = with_move.score_legal_done(move.get_trace(), new_indices, self._tile_vals)
        bonus_score = self._get_bonus_score_legal(new_indices)
        total_orth_score = self._get_total_orth_score_legal(move, new_indices)
        score = main_word_score + bonus_score + total_orth_score

        return score

    def _get_total_orth_score_legal(self, move, new_indices):
        """
        :param move: must be legal
        :param new_indices: the indices in the word being played that need to be added this move
        :return: total score of all the perpendicular words formed
        """

        indexed_orth_traces = self._board.get_indexed_orth_traces(move)
        with_move = self._board.try_move(move, False)  # orth score defined even if word doesn't connect

        total_orth_score = 0
        for trace, index_in_main, index_in_orth in indexed_orth_traces:
            if index_in_main in new_indices:
                # this orthogonal word is being formed by a letter played this turn (index in main word)
                total_orth_score += with_move.score_legal_done(trace, {index_in_orth}, self._tile_vals)
        return total_orth_score

    def _get_bonus_score_legal(self, new_indices):
        assert len(new_indices) > 0
        return 35 if len(new_indices) == self._num_tiles_to_hold else 0

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return '\ncurrent board:\n' + str(self._board)

def get_cmd_file_inp(io):
    def verify(raw):
        return os.path.isfile(raw)
    return io.raw_input_verified('file? ', verify)

def anagrams(dic):
    io = RawInputCommentPrint()
    tree = WWFTree(dic)
    while True:
        inp = io.raw_input('word to anagram:\n')
        nones = []
        spaces = ''
        for i in xrange(2*len(inp)):
            nones.append(None)
            if i % 2 == 0:
                spaces += ' '
        tiles = inp + spaces
        for res in tree.get_words(fixed_tiles=nones, using_tiles=tiles):
            print res[0]
        if not io.yes_no_input('another?'):
            return

def main_loop(g, args, main_io):
    # first game
    ignore_dict = args.report_move_ignore_dict
    try:
        g.play(display=(not args.no_display), ignore_dict=ignore_dict)
    except GameOverException as e:
        print 'Game over:', e.message
    while True:
        try:
            new_game = main_io.yes_no_input('new game?')
            if new_game:
                g.reset()
                use_cmd_file = main_io.yes_no_input('start from command file?')
                if use_cmd_file:
                    inp = get_cmd_file_inp(main_io)
                    print 'setting io'
                    g.set_io(RawInputCommentPrint(inp))
                else:
                    g.set_io(RawInputCommentPrint())
                g.play(display=(not args.no_display), ignore_dict=ignore_dict)
            else:
                break
        except QuitException:
            return 0
        except GameOverException as e:
            print 'Game over:', e.message
            continue
        except Exception as e:
            logging.error('game REPL encountered unknown error: {0}\n'.format(e.message))
            return 1
    return 0

def get_args():
    parser = ArgumentParser()
    parser.add_argument('--anagram', '-a', action='store_true')
    parser.add_argument('dictionary_file')
    parser.add_argument("--no-display", '-nd', action='store_true')
    parser.add_argument("--logging", '-l', type=str,
                        choices=['debug', 'info', 'warning', 'error',
                                 'critical'], default='info')
    parser.add_argument("--command_file", '-c', type=str)
    parser.add_argument(
        "--report-move-ignore-dict", "-i", action="store_true", default=False,
        help="allow reporting moves even if word not in dict"
    )
    parser.set_defaults(display=False)
    return parser.parse_args()

def get_dic(args):
    with open(args.dictionary_file, 'r') as dic_file:
        return set([line.strip() for line in dic_file])

def run_game(dic, args, io):
    me = Player('you')
    opponent = Player('opponent', wants_recommendations=False, infinite_bag=True)
    g = Game(dic, [me, opponent], io, num_recommendations=5)
    return main_loop(g, args, io)

def main():
    args = get_args()
    logging.basicConfig(level=getattr(logging, args.logging.upper()))
    dic = get_dic(args)

    if args.anagram:
        res = anagrams(dic)
    else:
        try:
            io = RawInputCommentPrint(args.command_file)
        except IOError as e:
            sys.stderr.write('fatal: IOError\n')
            return 1
        res = run_game(dic, args, io)

    print 'exiting'
    return res


if __name__ == '__main__':
    sys.exit(main())
