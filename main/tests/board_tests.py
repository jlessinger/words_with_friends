import unittest

from main.wwf import Board, Move


class BoardTests(unittest.TestCase):
    def setUp(self):
        pass

    def test_get_indexed_orth_traces(self):
        b = Board(15, [], [], [], [])
        or_move = Move('or', 'or', 'h', (8, 6))
        b.do_move(Move('ton', 'ton', 'h', (7, 6)), False)
        indexed_orth_traces = b.get_indexed_orth_traces(or_move)

        self.assertEqual(len(indexed_orth_traces), 2)

if  __name__ == '__main__':
    unittest.main()