import logging
import unittest
from subprocess import Popen, PIPE

class GameTests(unittest.TestCase):
    def setUp(self):
        self._test_dict_1 = 'tiny_dict.txt'
        self._real_dict = 'wwf_dict_real.txt'
        self._loren_dict = 'loren_dict.txt'

    def run_cmd(self, cmd, dic):
        p = Popen(['python', '../wwf.py', dic, '-nd', '-l', 'info'], \
                  stdout=PIPE, stdin=PIPE, stderr=PIPE)
        return p.communicate(input=cmd)[0]

    def test_simple_1(self):
        with open('simple.in', 'r') as f:
            cmd = f.read()
        print self.run_cmd(cmd, self._test_dict_1)
        self.assertTrue(False)

    def test_orth_1(self):
        with open('orth_1.in', 'r') as f:
            cmd = f.read()
        print self.run_cmd(cmd, self._test_dict_1)
        self.assertTrue(False)

    def test_loren_1(self):
        with open('loren.txt', 'r') as f:
            cmd = f.read()
            self.run_cmd(cmd, self._loren_dict)

if  __name__ == '__main__':
    unittest.main()