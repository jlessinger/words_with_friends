import os
import sys
from collections import Counter

from colorama import init as color_init, Style
color_init()

def colored(s, fore, back):
    return fore + back + s + Style.RESET_ALL

def spells_word(word, tiles):
    if len(tiles) != len(word):
        return False
    for w, t in zip(word, tiles):
        if (w != t) and (t != '*'):
            return False
    return True


class RawInputCommentPrint(object):
    def __init__(self, file_path=None):
        self._file_path = file_path
        if file_path is None or not os.path.exists(file_path):
            self._file_lines = []
        else:
            # open for append if exists else create new!!!!
            with open(file_path, 'r') as f:
                self._file_lines = [l for l in f]

    def yes_no_input(self, prompt):
        full_prompt = prompt + ' [y/n] '
        def verify(raw):
            return raw.lower() in ['y', 'ye', 'yes', 'n', 'no']
        inp = self.raw_input_verified(full_prompt, verify)
        return inp.startswith('y')

    def raw_input_verified(self, prompt, success_func):
        while True:
            raw = self.raw_input(prompt)
            if success_func(raw):
                return raw
            else:
                print 'try again'

    def raw_input(self, prompt=None):
        if prompt is None:
            prompt = ''
        if len(self._file_lines) == 0:
            try:
                inp = raw_input(prompt).strip()
            except EOFError:
                inp = ''
            if not os.isatty(sys.stdin.fileno()):
                print inp
            read_stdin = True
        else:
            inp = self._file_lines[0].strip()
            self._file_lines = self._file_lines[1:]
            sys.stdout.write(prompt)
            print inp
            read_stdin = False
        result = inp.split('#')[0].strip()
        if read_stdin and self._file_path is not None:
            with open(self._file_path, 'a+') as f:
                f.write('{0}\n'.format(result))
        if result.startswith('\\q'):
            raise QuitException()
        return result

    def has_file_input(self):
        return len(self._file_lines) > 0


class NoLegalMovesException(Exception):
    def __init__(self,*args,**kwargs):
        Exception.__init__(self,*args,**kwargs)

class IllegalMoveException(Exception):
    def __init__(self,*args,**kwargs):
        Exception.__init__(self,*args,**kwargs)

class IllegalMoveError(Exception):
    def __init__(self,*args,**kwargs):
        Exception.__init__(self,*args,**kwargs)

class GameOverException(Exception):
    def __init__(self,*args,**kwargs):
        Exception.__init__(self,*args,**kwargs)

class QuitException(Exception):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, **kwargs)

class Bag(Counter):
    def __init__(self, iter_or_mapping=[]):
        super(Bag, self).__init__(iter_or_mapping)

    def __add__(self, other):
        return Bag(super(Bag, self).__add__(other))

    def __sub__(self, other):
        return Bag(super(Bag, self).__sub__(other))

    def copy(self):
        return Bag(self)

    def contains(self, other):
        try:
            return all([other[e] <= self[e] for e in other])
        except KeyError:
            return False

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        if len(self) == 0:
            return '[Empty bag]'
        items = sum([[el] * self[el] for el in self],[])
        return ','.join(items)

class SmartHashable(object):
    def __init__(self, ignore_attrs=set([])):
        self._ignore = ignore_attrs.union(self._get_methods()).union({'_ignore', '_important'})
        self._important = None

    def _get_methods(self):
        return set([method for method in dir(self) if callable(getattr(self, method))])

    def _resolve_important(self):
        return [getattr(self, d) for d in self._important]

    def __hash__(self):
        if self._important is None:
            self._important = [d for d in dir(self) if d not in self._ignore
                                     and not d.startswith('__')]
        return hash(tuple(self._resolve_important()))

    def __eq__(self, other):
        if not isinstance(other, SmartHashable):
            return False
        return all([t[0] == t[1] for t in
                    zip(self._resolve_important(), other._resolve_important())])

    def __ne__(self, other):
        return not self.__eq__(other)


if __name__ == '__main__':
    held = Bag('haveloc')
    to_play = Bag('haveloc')
    print held.contains(to_play)

